package com.itau.jogovelha.controller;

import com.itau.jogovelha.model.Jogada;
import com.itau.jogovelha.model.Placar;
import com.itau.jogovelha.model.Rodada;
import com.itau.jogovelha.model.Send;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

//import com.itau.jogovelha.model.Tabuleiro;

@Controller
@CrossOrigin
public class JogoController {

	Rodada rodada = new Rodada("Jogador 1", "Jogador 2");

	@RequestMapping("/")
	public @ResponseBody
	Placar getPlacar() {
		return rodada.getPlacar();
	}

    @RequestMapping(path = "/", method = RequestMethod.POST)
    public @ResponseBody
    Placar jogar(@RequestBody Jogada jogada) {
    	
        String mensagem = "Clicou na posicao " + jogada.x + ", " + jogada.y;
        try {
        	Send.enviaMensagem(mensagem);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
        
        rodada.jogar(jogada.x, jogada.y);
        
        
        Placar placar = rodada.getPlacar();
        
//        try {
//        	Send.enviaMensagem(placar.pontuacao[0] + "x" + placar.pontuacao[1]);
//		} catch (Exception e) {
//			// TODO: handle exception
//			e.printStackTrace();
//		}
        
        return placar ;
    }

    @RequestMapping("/iniciar")
    public @ResponseBody
    Placar iniciarJogo() {
        rodada.iniciarJogo();
        
        String mensagem = "Clicou no botao iniciar novo jogo";
        try {
        	Send.enviaMensagem(mensagem);
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

        return rodada.getPlacar();
    }
}
