package com.itau.jogovelha.model;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;

public class Send {
	
	private final static String QUEUE_NAME = "mtech";

	  public static void enviaMensagem(String mensagem) throws Exception {
	    ConnectionFactory factory = new ConnectionFactory();
	    factory.setHost("localhost");
	    Connection connection = factory.newConnection();
	    Channel channel = connection.createChannel();

	    channel.queueDeclare(QUEUE_NAME, false, false, false, null);
	    channel.basicPublish("", QUEUE_NAME, null, mensagem.getBytes("UTF-8"));
	    System.out.println(" [x] Sent '" + mensagem + "'");

	    channel.close();
	    connection.close();
	}

}
